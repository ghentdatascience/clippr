## CLIPPR: Maximally Informative CLIPped PRojections with Bounding Regions
This code is developed along with paper "CLIPPR: Maximally Informative CLIPped PRojections with Bounding Regions". The code is developed and tested using MacOS Version 10.13.3.

## Dependencies
* TensorFlow
* pyManopt
* Numpy
* Scipy
* Matplotlib

## How to run
The users can follow the commands in script "run.sh" to reproduce the results and figures reported in the paper.

With TensorFlow activated, run single case study (with name "run_**.py" under folder "experiments") using simple command:

`python run_**.py`

The user can also run collectively using shell command:

`sh run.sh`
