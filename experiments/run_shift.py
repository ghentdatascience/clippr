import numpy as np
from computeW import computeW_circle
from computeW import computeW_ellipse
from computeW import computeW_rectangle
import matplotlib.pyplot as plt
from plotters import pca
from plotters import scatter
from plotters import scatter_bbox
from plotters import plot_bbox_ellipse
import pickle

def plot_results(X, Ws, As, Bs, Ms, output):
    fig, axarr = plt.subplots(1,3)
    annotations = ['(a)', '(b)', '(c)']

    # plot data
    scatter(axarr[0], X, annotations[0])

    # plot first result
    i = 0
    W = Ws[i]
    a = As[i]
    b = Bs[i]
    ms = Ms[i]

    XW = X.dot(W)
    inI = ((XW-ms)**2).dot([a,b]) <= 1
    outI = ((XW-ms)**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:] - ms
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None] + ms
    plot_bbox_ellipse(axarr[1], a, b, center=ms)
    scatter_bbox(axarr[1], inXW, outXW, inI, outI, annotations[1])

    #plot second result
    i = 3
    W = Ws[i]
    a = As[i]
    b = Bs[i]
    ms = Ms[i]
    XW = X.dot(W)
    inI = ((XW-ms)**2).dot([a,b]) <= 1
    outI = ((XW-ms)**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:] - ms
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None] + ms
    plot_bbox_ellipse(axarr[2], a, b, center=ms)
    scatter_bbox(axarr[2], inXW, outXW, inI, outI, annotations[2])

    for i in range(3):
        axarr[i].tick_params(axis='both', which='major', labelsize=10)
        axarr[i].tick_params(axis='both', which='minor', labelsize=10)

    fig.set_size_inches(12, 4)
    plt.tight_layout(w_pad=0.1)
    plt.savefig(output)
    plt.close()

def run_shift(fnum = 10):
    X = np.loadtxt(open('../data/shift.csv','rb'), delimiter=',')
    n,d = X.shape
    # X = X - X.mean(axis=0)
    k = 2
    sigma = np.sqrt(np.sum(np.var(X,0))/d)

    W = pca(X)

    Ws = []
    vals = []
    As = []
    Bs = []
    fs = []
    Ms = []

    fstart = 0.01
    fend = 1
    fstep = (fend-fstart)/fnum
    f = fstart
    meanAcc = X.mean(axis=0)
    for i in range(fnum+1):
        W, val, a, b = computeW_ellipse(X, k, f, sigma, W)

        XW = X.dot(W)
        inI = (XW**2).dot([a,b]) <= 1
        outI = (XW**2).dot([a,b]) > 1
        XW[outI,:] = XW[outI,:]/(((XW[outI,:]**2).dot([a,b]))**.5)[:,None]

        meanClippedXW = XW.mean(axis=0)
        meanClippedX = W.T.dot(meanClippedXW)

        Ws.append(W)
        vals.append(val)
        As.append(a)
        Bs.append(b)
        fs.append(f)
        Ms.append(meanAcc)

        X = X - meanClippedX
        meanAcc = meanAcc + meanClippedX
        f = f + fstep

    with open("../results/shift/parameters.pickle","wb") as file:
        pickle.dump(Ws, file)
        pickle.dump(vals, file)
        pickle.dump(As, file)
        pickle.dump(Bs, file)
        pickle.dump(fs, file)
        pickle.dump(Ms, file)
        pickle.dump(sigma, file)

    with open("../results/shift/parameters.pickle", "rb") as file:
        Ws = pickle.load(file)
        vals = pickle.load(file)
        As = pickle.load(file)
        Bs = pickle.load(file)
        fs = pickle.load(file)
        Ms = pickle.load(file)
        sigma = pickle.load(file)

    plot_results(X, Ws, As, Bs, Ms, "../results/shift/shift.pdf")

run_shift()
print('shift ellipse...done')
