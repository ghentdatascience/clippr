#!/bin/bash

# module load numpy
# module load Tensorflow
# module load matplotlib/2.0.1-intel-2017a-Python-3.6.1

mkdir -p ../results
mkdir -p ../results/datasaurus
mkdir -p ../results/segment
mkdir -p ../results/shuttle
mkdir -p ../results/shift
mkdir -p ../results/occlusion

python run_datasaurus.py
python run_segment.py
python run_shift.py
python run_occlusion.py
python run_shuttle.py
python run_shuttle_slide.py
