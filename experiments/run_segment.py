import numpy as np
from computeW import computeW_circle
from computeW import computeW_ellipse
from computeW import computeW_rectangle
import matplotlib.pyplot as plt
from plotters import pca
from plotters import scatter
from plotters import scatter_bbox
from plotters import plot_bbox_ellipse
import pickle

def plot_results(X, W, a, b, labels, classNames, output):
    XW = X.dot(W)
    fig, axarr = plt.subplots(1,4)
    annotations = ['(a)', '(b)', '(c)', '(d)']

    inI = (XW**2).dot([a,b]) <= 1
    outI = (XW**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]

    # plot without clipping
    plot_bbox_ellipse(axarr[0], a, b)
    scatter(axarr[0], XW, annotations[0], labels=labels, classNames=classNames, hasLegend=True)

    # plot with clipping
    plot_bbox_ellipse(axarr[1], a, b)
    scatter_bbox(axarr[1], inXW, outXW, inI, outI, annotations[1], labels=labels)

    # plot mds result
    mdsX = np.loadtxt(open('../data/segmentMDS.csv','rb'), delimiter=',')
    scatter(axarr[2], mdsX, annotations[2], labels=labels)

    # plot tsne result
    tsneX = np.loadtxt(open('../data/segmentTSNE.csv','rb'), delimiter=',')
    scatter(axarr[3], tsneX, annotations[3], labels=labels)


    for i in range(4):
        axarr[i].tick_params(axis='both', which='major', labelsize=10)
        axarr[i].tick_params(axis='both', which='minor', labelsize=10)

    fig.set_size_inches(16, 4)
    plt.tight_layout(w_pad=0.1)
    plt.savefig(output)
    plt.close()

def run_segment():
    X = np.loadtxt(open('../data/segment.csv','rb'), delimiter=',')
    n,d = X.shape
    [X, labels] = np.hsplit(X, np.array([d-1]))
    X = X - X.mean(axis=0)
    classNames = ['brickface', 'sky', 'foliage', 'cement', 'window', 'path', 'grass']
    d = d-1
    k = 2
    f = 0.08

    sigma = np.sqrt(np.sum(np.var(X,0))/d)

    W = pca(X)
    W, val, a, b = computeW_ellipse(X, k, f, sigma, W)

    with open("../results/segment/parameters.pickle","wb") as file:
        pickle.dump(W, file)
        pickle.dump(val, file)
        pickle.dump(a, file)
        pickle.dump(b, file)
        pickle.dump(f, file)
        pickle.dump(sigma, file)

    with open("../results/segment/parameters.pickle", "rb") as file:
        W = pickle.load(file)
        val = pickle.load(file)
        a = pickle.load(file)
        b = pickle.load(file)
        f = pickle.load(file)
        sigma = pickle.load(file)

    plot_results(X, W, a, b, labels, classNames, "../results/segment/segment.pdf")

run_segment()
print('segment ellipse...done')
