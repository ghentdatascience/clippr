import numpy as np
from computeW import computeW_circle
from computeW import computeW_ellipse
from computeW import computeW_rectangle
import matplotlib.pyplot as plt
from plotters import pca
from plotters import scatter
from plotters import scatter_bbox
from plotters import plot_bbox_ellipse
import pickle

def plot_results(X, Ws, As, Bs, labels, classNames, output):
    fig, axarr = plt.subplots(2,3)
    annotations = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']

    for i in range(len(Ws)):
        handle = axarr[int(i/3)][int(i%3)]
        W = Ws[i]
        a = As[i]
        b = Bs[i]
        XW = X.dot(W)

        inI = (XW**2).dot([a,b]) <= 1
        outI = (XW**2).dot([a,b]) > 1
        inXW = XW[inI,:]
        outXW = XW[outI,:]
        outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]

        # plot with clipping
        plot_bbox_ellipse(handle, a, b)
        scatter_bbox(handle, inXW, outXW, inI, outI, annotations[i], labels=labels, smallMarker=True)

        handle.tick_params(axis='both', which='major', labelsize=10)
        handle.tick_params(axis='both', which='minor', labelsize=10)

    fig.set_size_inches(12, 8)
    plt.savefig(output)
    plt.close()

def run_shuttle_slide():
    X = np.loadtxt(open('../data/shuttle.csv','rb'), delimiter=',')
    n,d = X.shape
    [X, labels] = np.hsplit(X, np.array([d-1]))
    X = X - X.mean(axis=0)
    classNames = ['Rad Flow', 'Fpv Close', 'Fpv Open', 'High', 'Bypass', 'Bpv Close', 'Bpv Open']
    d = d-1
    k = 2
    sigma = np.sqrt(np.sum(np.var(X,0))/d)

    W = pca(X)

    Ws = []
    vals = []
    As = []
    Bs = []
    fs = [0.01, 0.05, 0.06, 0.07, 0.2, 1.0]
    for i in range(len(fs)):
        print('iteration {}'.format(i))
        f = fs[i]
        W, val, a, b = computeW_ellipse(X, k, f, sigma, W)
        Ws.append(W)
        vals.append(val)
        As.append(a)
        Bs.append(b)

    with open("../results/shuttle/parameters_slide.pickle","wb") as file:
        pickle.dump(Ws, file)
        pickle.dump(vals, file)
        pickle.dump(As, file)
        pickle.dump(Bs, file)
        pickle.dump(fs, file)
        pickle.dump(sigma, file)

    with open("../results/shuttle/parameters_slide.pickle", "rb") as file:
        Ws = pickle.load(file)
        vals = pickle.load(file)
        As = pickle.load(file)
        Bs = pickle.load(file)
        fs = pickle.load(file)
        sigma = pickle.load(file)

    plot_results(X, Ws, As, Bs, labels, classNames, "../results/shuttle/shuttle_slide.pdf")

run_shuttle_slide()
print('shuttle ellipse slide...done')
