import numpy as np
from computeW import computeW_circle
from computeW import computeW_ellipse
from computeW import computeW_rectangle
import matplotlib.pyplot as plt
from plotters import pca
from plotters import scatter
from plotters import scatter_bbox
from plotters import plot_bbox_ellipse
import pickle

def plot_results(X, Ws, As, Bs, output):
    fig, axarr = plt.subplots(2,3)
    annotations = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']

    # plot data
    scatter(axarr[0][0], X, annotations[0])

    # plot mds result
    mdsX = np.loadtxt(open('../data/cluster_occlusionMDS.csv','rb'), delimiter=',')
    scatter(axarr[0][1], mdsX, annotations[1])

    # plot tsne result
    tsneX = np.loadtxt(open('../data/cluster_occlusionTSNE.csv','rb'), delimiter=',')
    scatter(axarr[0][2], tsneX, annotations[2])

    # plot first result
    i = 0
    W = Ws[i]
    a = As[i]
    b = Bs[i]
    XW = X.dot(W)
    inI = (XW**2).dot([a,b]) <= 1
    outI = (XW**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]
    plot_bbox_ellipse(axarr[1][0], a, b)
    scatter_bbox(axarr[1][0], inXW, outXW, inI, outI, annotations[3])

    # plot first result
    i = 4
    W = Ws[i]
    a = As[i]
    b = Bs[i]
    XW = X.dot(W)
    inI = (XW**2).dot([a,b]) <= 1
    outI = (XW**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]
    plot_bbox_ellipse(axarr[1][1], a, b)
    scatter_bbox(axarr[1][1], inXW, outXW, inI, outI, annotations[4])

    # plot first result
    i = 7
    W = Ws[i]
    a = As[i]
    b = Bs[i]
    XW = X.dot(W)
    inI = (XW**2).dot([a,b]) <= 1
    outI = (XW**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]
    plot_bbox_ellipse(axarr[1][2], a, b)
    scatter_bbox(axarr[1][2], inXW, outXW, inI, outI, annotations[5])


    for i in range(2):
        for j in range(3):
            axarr[i][j].tick_params(axis='both', which='major', labelsize=10)
            axarr[i][j].tick_params(axis='both', which='minor', labelsize=10)

    fig.set_size_inches(12, 8)
    plt.tight_layout(w_pad=0.1)
    plt.savefig(output)
    plt.close()

def run_occlusion():
    X = np.loadtxt(open('../data/occlusion.csv','rb'), delimiter=',')
    n,d = X.shape
    X = X - X.mean(axis=0)
    k = 2
    sigma = np.sqrt(np.sum(np.var(X,0))/d)

    W = pca(X)

    Ws = []
    vals = []
    As = []
    Bs = []
    Ms = []
    fs = [0.01, 0.1, 0.2, 0.4, 0.5, 0.7, 0.8, 1.0]

    for i in range(len(fs)):
        f = fs[i]
        W, val, a, b = computeW_ellipse(X, k, f, sigma, W)
        Ws.append(W)
        vals.append(val)
        As.append(a)
        Bs.append(b)
        fs.append(f)

    with open("../results/occlusion/parameters.pickle","wb") as file:
        pickle.dump(Ws, file)
        pickle.dump(vals, file)
        pickle.dump(As, file)
        pickle.dump(Bs, file)
        pickle.dump(fs, file)
        pickle.dump(sigma, file)

    with open("../results/occlusion/parameters.pickle", "rb") as file:
        Ws = pickle.load(file)
        vals = pickle.load(file)
        As = pickle.load(file)
        Bs = pickle.load(file)
        fs = pickle.load(file)
        sigma = pickle.load(file)

    plot_results(X, Ws, As, Bs, "../results/occlusion/occlusion.pdf")

run_occlusion()
print('occlusion ellipse...done')
