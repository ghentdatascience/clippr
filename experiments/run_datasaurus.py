import numpy as np
from computeW import computeW_circle
from computeW import computeW_ellipse
from computeW import computeW_rectangle
import matplotlib.pyplot as plt
from plotters import pca
from plotters import scatter
from plotters import scatter_bbox
from plotters import plot_bbox_ellipse
from plotters import plot_bbox_rectangle
import pickle

def clip(x, c):
    return max(-c, min(c, x))

def plot_results(X, Welli, a, b, Wrect, cs, Is, output):

    fig, axarr = plt.subplots(1,4)
    annotations = ['(a)', '(b)', '(c)', '(d)']

    XW = X.dot(Welli)
    # ellipse: plot without clipping
    inI = (XW**2).dot([a,b]) <= 1
    outI = (XW**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]
    outIdx = np.where(outI)[0]
    for i in range(6):
        axarr[2].plot([XW[outIdx[i],0], outXW[i, 0]], [XW[outIdx[i],1], outXW[i,1]], 'b--', linewidth=0.5)
    plot_bbox_ellipse(axarr[2], a, b)
    scatter(axarr[2], XW, annotations[2])

    # ellipse: plot with clipping
    plot_bbox_ellipse(axarr[3], a, b)
    scatter_bbox(axarr[3], inXW, outXW, inI, outI, annotations[3], hasLegend=True)

    XW = X.dot(Wrect)
    # rectangle: without clipping
    tempXW = XW.copy()
    tempXW[np.where(tempXW[:,0] > cs[0]), 0] = cs[0]
    tempXW[np.where(tempXW[:,0] < -cs[0]), 0] = -cs[0]
    tempXW[np.where(tempXW[:,1] > cs[1]), 1] = cs[1]
    tempXW[np.where(tempXW[:,1] < -cs[1]), 1] = -cs[1]
    inI = list(set(Is[0]).intersection(*Is))
    outI = list(set(range(len(tempXW))) - set(inI))
    inXW = tempXW[inI,:]
    outXW = tempXW[outI,:]

    for i in range(6):
        axarr[0].plot([XW[i+142,0], outXW[i, 0]], [XW[i+142,1], outXW[i, 1]], 'b--', linewidth=0.5)
    plot_bbox_rectangle(axarr[0], cs)
    scatter(axarr[0], XW, annotations[0])

    # rectangle: with clipp
    plot_bbox_rectangle(axarr[1], cs)
    scatter_bbox(axarr[1], inXW, outXW, inI, outI, annotations[1], hasLegend=False)

    for i in range(4):
        axarr[i].spines['left'].set_visible(False)
        axarr[i].spines['bottom'].set_visible(False)

        axarr[i].tick_params(axis='both', which='major', labelsize=10)
        axarr[i].tick_params(axis='both', which='minor', labelsize=10)

    fig.set_size_inches(16, 4)
    plt.tight_layout()
    plt.savefig(output)
    plt.close()

def run_datasaurus():
    X = np.loadtxt(open('../data/DatasaurusDozen_out.csv','rb'), delimiter=',')
    n,d = X.shape
    X = X - X.mean(axis=0)
    k = 2
    sigma = np.sqrt(np.sum(np.var(X,0))/d)
    f = 0.0001

    Welli, valE, a, b = computeW_ellipse(X, k, f, sigma, pca(X))
    with open("../results/datasaurus/parameters_ellipse.pickle","wb") as file:
        pickle.dump(Welli, file)
        pickle.dump(valE, file)
        pickle.dump(a, file)
        pickle.dump(b, file)
        pickle.dump(f, file)
        pickle.dump(sigma, file)

    Wrect, valR, cs, Is = computeW_rectangle(X, k, f, sigma, pca(X))
    with open("../results/datasaurus/parameters_rectangle.pickle","wb") as file:
        pickle.dump(Wrect, file)
        pickle.dump(valR, file)
        pickle.dump(cs, file)
        pickle.dump(Is, file)
        pickle.dump(f, file)
        pickle.dump(sigma, file)

    plot_results(X, Welli, a, b, Wrect, cs, Is, "../results/datasaurus/datasaurus.pdf")


run_datasaurus()
print('datasaurus...done')
