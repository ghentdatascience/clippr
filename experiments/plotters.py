import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as colors

def scatter(handle, XW, annotation, labels=None, classNames=None, hasLegend=False, smallMarker=False):
    if smallMarker:
        s = 1
    else:
        s = 3
    if labels is None:
        handle.scatter(XW[:,0], XW[:,1], marker='o', facecolor='0.5',s = s)
    else:
        uniqueLabels = list(set(labels.flatten()))
        norm = colors.Normalize(vmin=1.,vmax=len(uniqueLabels))
        if classNames is None:
            classNames = range(len(uniqueLabels))
        for i in range(len(uniqueLabels)):
            classInds = np.where(labels == uniqueLabels[i])[0]
            handle.scatter(XW[classInds,0], XW[classInds,1], marker='o', facecolor='0.5',s = s, c=labels[classInds],norm=norm,cmap = 'jet', label=classNames[i])
    handle.spines['right'].set_visible(False)
    handle.spines['top'].set_visible(False)
    handle.yaxis.set_ticks_position('left')
    handle.xaxis.set_ticks_position('bottom')
    xlim = handle.get_xlim()
    ylim = handle.get_ylim()
    handle.annotate(annotation, xy=(xlim[0], ylim[1]),  fontsize=15)

    if hasLegend:
        leg = handle.legend(borderpad=0.1, labelspacing=0.1, fontsize=15)
        cmap = matplotlib.cm.get_cmap('jet')
        for i in range(len(leg.legendHandles)):
            leg.legendHandles[i].set_color(cmap(norm(i+1)))

def scatter_bbox(handle, inXW, outXW, inI, outI, annotation, labels=None, hasLegend=False, hasEqualAspect=False, smallMarker=False):
    if smallMarker:
        sIn = 1
        sOut = 10
    else:
        sIn = 2
        sOut = 15
    if labels is None:
        handle.scatter(inXW[:,0], inXW[:,1], marker='o', facecolor='0.5', s = sIn, label='inside')
        handle.scatter(outXW[:,0], outXW[:,1], marker='^', facecolor='0.5', s = sOut, label='outside')
    else:
        uniqueLabels = list(set(labels.flatten()))
        norm = colors.Normalize(vmin=1.,vmax=len(uniqueLabels))
        handle.scatter(inXW[:,0], inXW[:,1], marker='o', facecolor='0.5', s = sIn, c=labels[inI],norm=norm,cmap = 'jet', label='inside')
        handle.scatter(outXW[:,0], outXW[:,1], marker='^', facecolor='0.5', s = sOut, c=labels[outI],norm=norm,cmap = 'jet', label='outside')
    handle.spines['right'].set_visible(False)
    handle.spines['top'].set_visible(False)
    handle.yaxis.set_ticks_position('left')
    handle.xaxis.set_ticks_position('bottom')
    if hasEqualAspect:
        handle.set_aspect('equal')
    xlim = handle.get_xlim()
    ylim = handle.get_ylim()
    handle.annotate(annotation, xy=(xlim[0], ylim[1]),  fontsize=15)
    if hasLegend:
        leg = handle.legend(loc='upper left')

def plot_bbox_circle(handle, radius):
    circ = patches.Circle((0,0), radius=radius, linewidth=0.5,edgecolor='r',facecolor='none')
    handle.add_patch(circ)
    return handle

def plot_bbox_rectangle(handle, cs):
    rect = patches.Rectangle(np.multiply(cs, -1),cs[0]*2,cs[1]*2,linewidth=0.5,edgecolor='r',facecolor='none')
    handle.add_patch(rect)
    return handle

def plot_bbox_ellipse(handle, a, b, center=(0,0)):
    ellipse = patches.Ellipse(center, width=2/a**.5, height=2/b**.5, linewidth=0.5,edgecolor='r',facecolor='none')
    handle.add_patch(ellipse)

def plotObjofRs(handle, rvs, annotation):
    rvs = list(reversed(rvs))
    handle.plot(rvs, 'o', markersize=1, label='x-axis')
    optRidx, optRv = max(enumerate(rvs), key=operator.itemgetter(1))
    handle.scatter([optRidx], [optRv], s=50, facecolors='none', edgecolors='r', linewidth='0.75', label='optimum')
    handle.legend(loc='best')
    handle.spines['right'].set_visible(False)
    handle.spines['top'].set_visible(False)
    handle.yaxis.set_ticks_position('left')
    handle.xaxis.set_ticks_position('bottom')
    xlim = handle.get_xlim()
    ylim = handle.get_ylim()
    handle.set_xlabel('number of points inside the bouding box', fontsize=10)
    handle.set_ylabel('objective', fontsize=10)
    handle.annotate(annotation, xy=(xlim[0], ylim[1]))

def plotObjofCs(handle, cvs, ann):
    cvs[0] = list(reversed(cvs[0]))
    cvs[1] = list(reversed(cvs[1]))
    handle.plot(cvs[0], 'o', markersize=1, label='x-axis')
    handle.plot(cvs[1], 'o', markersize=1, label='y-axis')
    optCidx0, optCv0 = max(enumerate(cvs[0]), key=operator.itemgetter(1))
    optCidx1, optCv1 = max(enumerate(cvs[1]), key=operator.itemgetter(1))
    handle.scatter([optCidx0, optCidx1], [optCv0, optCv1], s=50, facecolors='none', edgecolors='r', linewidth='0.75', label='optimum')
    handle.legend()
    handle.spines['right'].set_visible(False)
    handle.spines['top'].set_visible(False)
    handle.yaxis.set_ticks_position('left')
    handle.xaxis.set_ticks_position('bottom')
    xlim = handle.get_xlim()
    ylim = handle.get_ylim()
    handle.set_xlabel('number of points inside the bouding box', fontsize=10)
    handle.set_ylabel('objective', fontsize=10)
    handle.annotate(ann, xy=(xlim[0], ylim[1]))

def pca(X):
    X = X - X.mean(axis=0)
    X.T.dot(X)
    eigVal, eigVec = np.linalg.eig(X.T.dot(X))
    pairs = [(np.abs(eigVal[i]), eigVec[:,i]) for i in range(len(eigVal))]
    pairs.sort(key=lambda x: x[0], reverse=True)
    pcaW = np.array([pairs[i][1] for i in range(len(pairs))]).T
    return pcaW[:,0:2]

def plot_result_ellipse(X, W, output, a, b, labels=None, classNames=None):
    XW = X.dot(W)
    fig, axarr = plt.subplots(1,3)
    annotations = ['(a)', '(b)', '(c)']
    hasLegend = False
    if classNames is not None:
        hasLegend = True

    scatter(axarr[0], X.dot(pca(X)), annotations[0])

    plot_bbox_ellipse(axarr[1], a, b)
    scatter(axarr[1], XW, annotations[1], labels=labels, classNames=classNames, hasLegend=hasLegend)

    plot_bbox_ellipse(axarr[2], a, b)
    inI = (XW**2).dot([a,b]) <= 1
    outI = (XW**2).dot([a,b]) > 1
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/(((outXW**2).dot([a,b]))**.5)[:,None]
    scatter_bbox(axarr[2], inXW, outXW, inI, outI, annotations[2], labels=labels)

    fig.set_size_inches(15, 5)
    plt.savefig(output)
    plt.close()

def plot_result_circle(X, W, output, radius, labels=None, classNames=None):
    XW = X.dot(W)
    fig, axarr = plt.subplots(1,3)
    annotations = ['(a)', '(b)', '(c)']
    hasLegend = False
    if classNames is not None:
        hasLegend = True
    scatter(axarr[0], X.dot(pca(X)), annotations[0], labels=labels, classNames=classNames, hasLegend=hasLegend)

    plot_bbox_circle(axarr[1], radius)
    scatter(axarr[1], XW, annotations[1], labels=labels)

    plot_bbox_circle(axarr[2], radius)
    inI = np.sum((XW**2), axis=1) <= radius**2
    outI = np.sum((XW**2), axis=1) > radius**2
    inXW = XW[inI,:]
    outXW = XW[outI,:]
    outXW = outXW/((np.sum(outXW**2, axis=1))**.5)[:,None]*radius
    scatter_bbox(axarr[2], inXW, outXW, inI, outI, annotations[2], labels=labels, hasEqualAspect=True)

    fig.set_size_inches(15, 5)
    plt.savefig(output)
    plt.close()

def plot_result_rectangle(X, W, output, cs, Is, labels=None, classNames=None):
    XW = X.dot(W)
    fig, axarr = plt.subplots(1,3)
    annotations = ['(a)', '(b)', '(c)']
    hasLegend = False
    if classNames is not None:
        hasLegend = True

    scatter(axarr[0], X.dot(pca(X)), annotations[0])

    plot_bbox_rectangle(axarr[1], cs)
    scatter(axarr[1], XW, annotations[1], labels=labels, classNames=classNames, hasLegend=hasLegend)

    plot_bbox_rectangle(axarr[2], cs)
    XW[np.where(XW[:,0] > cs[0]), 0] = cs[0]
    XW[np.where(XW[:,0] < -cs[0]), 0] = -cs[0]
    XW[np.where(XW[:,1] > cs[1]), 1] = cs[1]
    XW[np.where(XW[:,1] < -cs[1]), 1] = -cs[1]
    inI = list(set(Is[0]).intersection(*Is))
    outI = list(set(range(len(XW))) - set(inI))
    scatter_bbox(axarr[2], XW[inI,:], XW[outI,:], inI, outI, annotations[2], labels=labels)

    fig.set_size_inches(15, 5)
    plt.savefig(output)
    plt.close()
